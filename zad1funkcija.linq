<Query Kind="SQL">
  <Connection>
    <ID>d4f08406-449a-4ad8-9f73-99832b189121</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>LV4_primjer</Database>
  </Connection>
</Query>

CREATE FUNCTION reverseFunction(@string VARCHAR(31))
RETURNS VARCHAR(60)

BEGIN
DECLARE @pocetni VARCHAR(30)=@string;
DECLARE @temp varchar(60)='';

IF LEN(@string)>30
BEGIN
SET @temp='Ulazna riječ je predugačka, maksimalan broj znakova je 30'
RETURN @temp
END

WHILE LEN(@string)>0
BEGIN
SET @temp=@temp+SUBSTRING(@string,LEN(@string),1);
SET @string=SUBSTRING(@string,1,LEN(@string)-1)
END

IF UPPER(@pocetni)=UPPER(@temp)
BEGIN
SET @temp='palindrom'
END

RETURN @temp
END




CREATE FUNCTION reverseFunction2(@string VARCHAR(31))
RETURNS VARCHAR(60)

BEGIN
DECLARE @pocetni VARCHAR(30)=@string;
DECLARE @temp varchar(60)='';

IF LEN(@string)>30
BEGIN
SET @temp='Ulazna riječ je predugačka, maksimalan broj znakova je 30'
RETURN @temp
END

SET @temp=REVERSE(@string);

IF UPPER(@pocetni)=UPPER(@temp)
BEGIN
SET @temp='palindrom';
END

RETURN @temp
END

select dbo.reverseFunction2('ana') 