<Query Kind="SQL">
  <Connection>
    <ID>d4f08406-449a-4ad8-9f73-99832b189121</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>LV4_primjer</Database>
  </Connection>
</Query>


INSERT INTO stavke_racuna VALUES(1, 1, 10, 'kom', 'jogurt', 2, NULL);
INSERT INTO stavke_racuna VALUES(1, 2, 12, 'kom', 'mlijeko', 2.5, NULL);
INSERT INTO stavke_racuna VALUES(1, 3, 5, 'kom', 'kruh', 4, NULL);

drop trigger IzracunUkupno
CREATE TRIGGER IzracunUkupno
ON stavke_racuna
FOR INSERT, UPDATE
AS
DECLARE @broj_racuna INT;
DECLARE @rb INT;
SELECT @broj_racuna = broj_racuna, @rb = rb
 FROM inserted;

UPDATE stavke_racuna SET ukupno = kolicina * cijena * 1.25 
WHERE broj_racuna in(@broj_racuna) and rb in(@rb);


SELECT * FROM stavke_racuna;
SELECT * FROM racuni;
UPDATE stavke_racuna SET kolicina = 10 WHERE broj_racuna = 1 AND rb in(2,3);