<Query Kind="SQL">
  <Connection>
    <ID>d4f08406-449a-4ad8-9f73-99832b189121</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>LV4_primjer</Database>
  </Connection>
</Query>

CREATE TRIGGER reverseInsert
ON racuni
INSTEAD OF INSERT
AS
DECLARE @string CHAR(50);
DECLARE @broj INT;
DECLARE @datum DATETIME;
SELECT @string=kupac,@broj=broj, @datum=datum FROM inserted;
DECLARE @temp char(50);

SET @temp=REVERSE(@string);
INSERT INTO racuni VALUES(@broj,@datum, @temp);

drop trigger reverseInsert
delete from racuni where datum='01/01/2001'
INSERT INTO racuni VALUES(3, '01/01/2001', 'Lora');