<Query Kind="SQL">
  <Connection>
    <ID>d4f08406-449a-4ad8-9f73-99832b189121</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>LV4_primjer</Database>
  </Connection>
</Query>

CREATE PROCEDURE reverseProcedure(@string VARCHAR(31))
AS

DECLARE @pocetni VARCHAR(30)=@string;
DECLARE @temp varchar(60)='';


SET @temp=REVERSE(@string);

IF UPPER(@pocetni)=UPPER(@temp)
BEGIN
SET @temp='palindrom';
END

IF LEN(@string)>30
BEGIN
SET @temp='Ulazna riječ je predugačka, maksimalan broj znakova je 30'
END

PRINT @temp

EXEC reverseProcedure 'Laura'
