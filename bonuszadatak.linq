<Query Kind="SQL">
  <Connection>
    <ID>d4f08406-449a-4ad8-9f73-99832b189121</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>LV4_primjer</Database>
  </Connection>
</Query>

CREATE PROCEDURE horoskop(@datum DATETIME)
AS
DECLARE @znak VARCHAR(9);
SET @datum = DATEFROMPARTS(2000, MONTH(@datum), DAY(@datum));
IF @datum BETWEEN'01/21/2000' AND '02/19/2000' 
BEGIN
SET @znak='vodenjak'
END
IF @datum BETWEEN'02/20/2000' AND '03/20/2000' 
BEGIN
SET @znak='ribe'
END
IF @datum BETWEEN'03/21/2000' AND '04/20/2000' 
BEGIN
SET @znak='ovan'
END
IF @datum BETWEEN'04/21/2000' AND '05/21/2000' 
BEGIN
SET @znak='bik'
END
IF @datum BETWEEN'05/22/2000' AND '06/21/2000' 
BEGIN
SET @znak='blizanci'
END
IF @datum BETWEEN'06/22/2000' AND '07/23/2000' 
BEGIN
SET @znak='rak'
END
IF @datum BETWEEN'07/24/2000' AND '08/22/2000' 
BEGIN
SET @znak='lav'
END
IF @datum BETWEEN'08/23/2000' AND '09/22/2000' 
BEGIN
SET @znak='djevica'
END
IF @datum BETWEEN'09/23/2000' AND '10/23/2000' 
BEGIN
SET @znak='vaga'
END
IF @datum BETWEEN'10/24/2000' AND '11/22/2000' 
BEGIN
SET @znak='skorpion'
END
IF @datum BETWEEN'11/23/2000' AND '12/21/2000' 
BEGIN
SET @znak='strijelac'
END
IF @datum >'12/22/2000' OR @datum<'01/20/2000' 
BEGIN
SET @znak='jarac'
END

PRINT @znak

exec horoskop '01/19/1971'